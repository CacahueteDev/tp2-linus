**SQL** : Structured Query Language, a.k.a. *mon cauchemar*.

> T'a des anglais qui l'appellent *Sequel* ! Wtf

## Introduction

**Base de données (database)** : 
	**SGBDR** : 
		**Maria DB**
		**MySQL**
		**SQL Server (🤮 Microsoft caca)**
		**sqlite**
	**Clé/valeur** : 
		**Mongo DB**
		**Redis**

On accède à une base de données avec un **client**.

### Kekesé

Une base de données est une collection de tables.
Chaque table est une liste d'items ayant des champs en communs (les colonnes). Chaque item a une **clé primaire**. 

Les bases de données traitent les données très rapidement (et plus rapidement qu'un programme classique). On peut les lier facilement (facilement ?!) entre elles.

**L'utilité de mettre la base de données sur un autre serveur que le serveur web** : Si le serveur web plante, la bdd peut être toujours active, etc...

Ne **jamais** donner l'accès à la bdd aux clients, uniquement au serveur, donc au ***backend***.

Vos **transactions banquaires** sont dans une base de données SQL ! (putain il me fait chier jusqu'a là ptdr !)

## Pratique

Commandes de base en (My)SQL :

### MySQL

```mysql
SHOW DATABASES;
```
Affiche toutes les bases de données du serveur.

```mysql
USE bdd;
```
Ouvrir la base de données `bdd`.

```mysql
SHOW TABLES;
```
Affiche les tables de la base de données

```mysql
DESCRIBE;
```
Décris les colonnes de la table

```mysql
CREATE USER 'chval'@'%' IDENTIFIED BY 'chvalsus';
```
Créer un utilisateur

```mysql
BYE
```
Se déconnecter du serveur

### SQL

```mysql
SELECT * FROM users;
```
Séléctionne tout dans la table `users`.

```mysql
SELECT name FROM users;
```
Séléctionne le *nom* de tout le monde dans la table `users`.

Point prof : faut éviter d'utiliser *sqlite*, personne l'utilise à part pour du perso.