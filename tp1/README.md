# II. Feu

### Trouver au moins 4 façons différentes de péter la machine

### 1. Remplacer /bin/bash par le programme "yes"
Oui

```
$ sudo rm -f /bin/bash
$ sudo cp /bin/yes /bin/bash
```

Eh bah ça démarre même plus...

### 2. Les symlinks
Un lien symbolique `a` qui redirige vers un lien symbolique `b`, qui redirige vers `a`.
Et au fait `a`, il s'appelle `/etc` maintenant !

```
$ cd /
$ su
# mkdir b
# ln -s b a
# rm -rf /etc
# mv a /etc
# reboot
```

Il n'y a plus d'utilisateur, et la VM démarre en *emergency mode*...

### 3. Une vanne dans un fichier aléatoire
Je vais placer une vanne dans un fichier ultra important : go trouver **le** fichier.

![gif roulette](https://media.discordapp.net/attachments/857636073891299359/1046728838853431326/roulette.gif)

### La roulette russe

**/bin/systemctl** est notre victime pour cette méga vanne !

Maintenant, faut trouver une vanne.
J'en ai piqué une sur un site en despi :
```
Que dit une mère à son fils geek quand le dîner est servi ?
Alt Tab !
```

Et ça part dans **/bin/systemctl**.
```
$ sudo nano /bin/systemctl
[j'introduis la vanne au milieu]
```

Puis j'ai redémarré
```
$ reboot
reboot[1369]: segfault at 8 ip agaa?f6b8c52868a sp agga?fff8ûc4c428 error 4 in ld-linux-x86-64.so.[...]
Code: 5b 4c 89 fa 5d 41 5c 41 5d 41 se c3 66 66 Ze Of If 84 aa aa aa aa aa Of If aa b
8 41 ff ff 6f 29 dB 48 8b 54 c6 48 48 8b 46 68 8b 48 88 f6 86 le 83 aa aa za 74 83 48 83 86 48
85 d2 74 Zb 48
Segmentation fault (core dumped)
```

**Résultat :** la VM marche toujours, sauf que beaucoup de commandes ne marchent plus à cause du **systemctl** déféctueux. Et puis les services en arrière plan, bisous !

### 4. Un petit caractère de changé dans Linuz
Juste un caractère, ça fait rien hein ?

J'ai donc fait un `nano` et j'ai édité un petit fichier appelé `vmlinuz` dans le dossier `/boot`.
J'introduis un **D** dans le fichier.

![D](https://upload.wikimedia.org/wikipedia/commons/thumb/2/24/D_Programming_Language_logo.svg/1200px-D_Programming_Language_logo.svg.png)

Résultat...

Le kernel n'est plus chargé :
+ **Invalid magic number**
+ **You need to load the kernel first**

Il veut que je charge le noyau moi même *il est fou* !

![](linus.png)