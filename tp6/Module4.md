### Installer Netdata

```sh
[chval@db ~]$ sudo firewall-cmd status
[sudo] password for chval:
usage: see firewall-cmd man page
firewall-cmd: error: unrecognized arguments: status
[chval@db ~]$ sudo firewall-cmd --state
running
[chval@db ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
-bash: wget: command not found
[chval@db ~]$ sudo dnf install wget
Last metadata expiration check: 0:26:26 ago on Sun 22 Jan 2023 03:30:41 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                 Architecture              Version                           Repository                    Size
========================================================================================================================
Installing:
 wget                    x86_64                    1.21.1-7.el9                      appstream                    769 k

Transaction Summary
========================================================================================================================
Install  1 Package

Total download size: 769 k
Installed size: 3.1 M
Is this ok [y/N]: y
Downloading Packages:
[MIRROR] wget-1.21.1-7.el9.x86_64.rpm: Curl error (28): Timeout was reached for http://rocky.reloumirrors.net/9.1/AppStream/x86_64/os/Packages/w/wget-1.21.1-7.el9.x86_64.rpm [Failed to connect to rocky.reloumirrors.net port 80: Connection timed out]
wget-1.21.1-7.el9.x86_64.rpm                                                             44 kB/s | 769 kB     00:17
------------------------------------------------------------------------------------------------------------------------
Total                                                                                    42 kB/s | 769 kB     00:18
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : wget-1.21.1-7.el9.x86_64                                                                       1/1
  Running scriptlet: wget-1.21.1-7.el9.x86_64                                                                       1/1
  Verifying        : wget-1.21.1-7.el9.x86_64                                                                       1/1

Installed:
  wget-1.21.1-7.el9.x86_64

Complete!
[chval@db ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
--2023-01-22 15:57:38--  https://my-netdata.io/kickstart.sh
Resolving my-netdata.io (my-netdata.io)... failed: Name or service not known.
wget: unable to resolve host address ‘my-netdata.io’
[chval@db ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
--2023-01-22 15:57:50--  https://my-netdata.io/kickstart.sh
Resolving my-netdata.io (my-netdata.io)... 188.114.97.2, 188.114.96.2, 2a06:98c1:3121::2, ...
Connecting to my-netdata.io (my-netdata.io)|188.114.97.2|:443... connected.
HTTP request sent, awaiting response... 200 OK
Length: unspecified [application/octet-stream]
Saving to: ‘/tmp/netdata-kickstart.sh’

/tmp/netdata-kickstart.sh         [ <=>                                              ]  79.27K  --.-KB/s    in 0.1s

2023-01-22 15:57:51 (746 KB/s) - ‘/tmp/netdata-kickstart.sh’ saved [81175]


 --- Using /tmp/netdata-kickstart-zknKSpu49j as a temporary directory. ---
 --- Checking for existing installations of Netdata... ---
 --- No existing installations of netdata found, assuming this is a fresh install. ---
 --- Attempting to install using native packages... ---
 --- Checking for availability of repository configuration package. ---
[/tmp/netdata-kickstart-zknKSpu49j]$ curl --fail -q -sSL --connect-timeout 10 --retry 3 --output /tmp/netdata-kickstart-zknKSpu49j/netdata-repo-edge-2-1.noarch.rpm https://repo.netdata.cloud/repos/repoconfig/el/9/x86_64/netdata-repo-edge-2-1.noarch.rpm
 OK

[/tmp/netdata-kickstart-zknKSpu49j]$ sudo env dnf install /tmp/netdata-kickstart-zknKSpu49j/netdata-repo-edge-2-1.noarch.rpm
Last metadata expiration check: 0:27:44 ago on Sun 22 Jan 2023 03:30:41 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                            Architecture            Version                 Repository                     Size
========================================================================================================================
Installing:
 netdata-repo-edge                  noarch                  2-1                     @commandline                  7.0 k

Transaction Summary
========================================================================================================================
Install  1 Package

Total size: 7.0 k
Installed size: 548
Is this ok [y/N]: y
Downloading Packages:
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : netdata-repo-edge-2-1.noarch                                                                   1/1
  Verifying        : netdata-repo-edge-2-1.noarch                                                                   1/1

Installed:
  netdata-repo-edge-2-1.noarch

Complete!
 OK

[/tmp/netdata-kickstart-zknKSpu49j]$ sudo env dnf makecache
Netdata Edge                                                                             50  B/s | 687  B     00:13
Netdata Edge                                                                            698  B/s | 2.4 kB     00:03
Importing GPG key 0x65F56346:
 Userid     : "Netdatabot <bot@netdata.cloud>"
 Fingerprint: 6588 FDD7 B147 21FE 7C31 15E6 F917 7B52 65F5 6346
 From       : https://repo.netdata.cloud/netdatabot.gpg.key
Is this ok [y/N]: y
Netdata Edge                                                                             57 kB/s | 123 kB     00:02
Netdata Repository Config                                                               747  B/s | 687  B     00:00
Netdata Repository Config                                                               7.3 kB/s | 2.4 kB     00:00
Importing GPG key 0x65F56346:
 Userid     : "Netdatabot <bot@netdata.cloud>"
 Fingerprint: 6588 FDD7 B147 21FE 7C31 15E6 F917 7B52 65F5 6346
 From       : https://repo.netdata.cloud/netdatabot.gpg.key
Is this ok [y/N]: y
Netdata Repository Config                                                               838  B/s | 1.4 kB     00:01
Extra Packages for Enterprise Linux 9 - x86_64                                          5.9 kB/s |  23 kB     00:03
Rocky Linux 9 - BaseOS                                                                  5.4 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                               5.8 kB/s | 4.1 kB     00:00
Rocky Linux 9 - Extras                                                                  4.4 kB/s | 2.9 kB     00:00
Metadata cache created.
 OK

Netdata Edge                                    103 kB/s | 123 kB     00:01
Netdata Repository Config                       1.1 kB/s | 1.4 kB     00:01
Extra Packages for Enterprise Linux 9 - x86_64  1.0 MB/s |  13 MB     00:12
Rocky Linux 9 - BaseOS                          797 kB/s | 1.7 MB     00:02
Rocky Linux 9 - AppStream                       1.1 MB/s | 6.4 MB     00:05
Rocky Linux 9 - Extras                          7.9 kB/s | 8.5 kB     00:01
[/tmp/netdata-kickstart-zknKSpu49j]$ sudo env dnf install netdata
Last metadata expiration check: 0:00:31 ago on Sun 22 Jan 2023 03:59:01 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                Architecture         Version                                   Repository                  Size
========================================================================================================================
Installing:
 netdata                x86_64               1.37.0.176.nightly-1.el9                  netdata-edge                38 M
Installing dependencies:
 protobuf               x86_64               3.14.0-13.el9                             appstream                  1.0 M

Transaction Summary
========================================================================================================================
Install  2 Packages

Total download size: 39 M
Installed size: 174 M
Is this ok [y/N]: y
Downloading Packages:
(1/2): protobuf-3.14.0-13.el9.x86_64.rpm                                                825 kB/s | 1.0 MB     00:01
(2/2): netdata-1.37.0.176.nightly-1.el9.x86_64.rpm                                      979 kB/s |  38 MB     00:40
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   991 kB/s |  39 MB     00:40
Netdata Edge                                                                            3.9 kB/s | 2.4 kB     00:00
Importing GPG key 0x65F56346:
 Userid     : "Netdatabot <bot@netdata.cloud>"
 Fingerprint: 6588 FDD7 B147 21FE 7C31 15E6 F917 7B52 65F5 6346
 From       : https://repo.netdata.cloud/netdatabot.gpg.key
Is this ok [y/N]: y
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : protobuf-3.14.0-13.el9.x86_64                                                                  1/2
  Running scriptlet: netdata-1.37.0.176.nightly-1.el9.x86_64                                                        2/2
  Installing       : netdata-1.37.0.176.nightly-1.el9.x86_64                                                        2/2
  Running scriptlet: netdata-1.37.0.176.nightly-1.el9.x86_64                                                        2/2
Created symlink /etc/systemd/system/multi-user.target.wants/netdata.service → /usr/lib/systemd/system/netdata.service.

  Verifying        : netdata-1.37.0.176.nightly-1.el9.x86_64                                                        1/2
  Verifying        : protobuf-3.14.0-13.el9.x86_64                                                                  2/2

Installed:
  netdata-1.37.0.176.nightly-1.el9.x86_64                         protobuf-3.14.0-13.el9.x86_64

Complete!
 OK

Sun Jan 22 04:00:21 PM CET 2023 : INFO: netdata-updater.sh:  Auto-updating has been ENABLED through cron, updater script linked to /etc/cron.daily/netdata-updater\n
Sun Jan 22 04:00:21 PM CET 2023 : INFO: netdata-updater.sh:  If the update process fails and you have email notifications set up correctly for cron on this system, you should receive an email notification of the failure.
Sun Jan 22 04:00:21 PM CET 2023 : INFO: netdata-updater.sh:  Successful updates will not send an email.
Successfully installed the Netdata Agent.

Official documentation can be found online at https://learn.netdata.cloud/docs/.

Looking to monitor all of your infrastructure with Netdata? Check out Netdata Cloud at https://app.netdata.cloud.

Join our community and connect with us on:
  - GitHub: https://github.com/netdata/netdata/discussions
  - Discord: https://discord.gg/5ygS846fR6
  - Our community forums: https://community.netdata.cloud/
[chval@db ~]$
```

J'ai ouvert le port : 
```sh
[chval@db ~]$ sudo firewall-cmd --add-port=19999/tcp --permanent
success
[chval@db ~]$ sudo firewall-cmd --reload
success
```

### Une fois Netdata installé et fonctionnel, déterminer :

L'utilisateur : `netdata`
Les ports sur lequels **netdata** écoute :
```sh
[chval@db ~]$ ss -lapute | grep netdata
udp   UNCONN 0      0                  127.0.0.1:8125           0.0.0.0:*      uid:990 ino:27298 sk:8 cgroup:/system.slice/netdata.service <->
udp   ESTAB  0      0                  127.0.0.1:37119        127.0.0.1:323    uid:990 ino:27570 sk:b cgroup:/system.slice/netdata.service <->
udp   UNCONN 0      0                      [::1]:8125              [::]:*      uid:990 ino:27297 sk:d cgroup:/system.slice/netdata.service v6only:1 <->
tcp   LISTEN 0      4096               127.0.0.1:8125           0.0.0.0:*      uid:990 ino:27303 sk:2 cgroup:/system.slice/netdata.service <->
tcp   LISTEN 0      4096                 0.0.0.0:dnp-sec        0.0.0.0:*      uid:990 ino:26429 sk:3 cgroup:/system.slice/netdata.service <->
tcp   ESTAB  0      0             192.168.56.112:dnp-sec 192.168.56.109:50942  timer:(keepalive,113min,0) uid:990 ino:30698 sk:f cgroup:/system.slice/netdata.service <->
tcp   LISTEN 0      4096                   [::1]:8125              [::]:*      uid:990 ino:27302 sk:6 cgroup:/system.slice/netdata.service v6only:1 <->
tcp   LISTEN 0      4096                    [::]:dnp-sec           [::]:*      uid:990 ino:26430 sk:7 cgroup:/system.slice/netdata.service v6only:1 <->
[chval@db ~]$
```

Il écoute sur :
+ 323/udp
+ 50942/tcp (et c'est une connexion établie)

Les logs de netdata sont dans `/var/log/netdata/` :
```
[chval@db ~]$ cd /var/log/netdata/
[chval@db netdata]$ ls -all
total 336
drwxr-xr-x.  2 netdata root        76 Jan 22 16:00 .
drwxr-xr-x. 10 root    root      4096 Jan 22 16:00 ..
-rw-r--r--.  1 netdata netdata  57463 Jan 22 16:11 access.log
-rw-r--r--.  1 netdata netdata      0 Jan 22 16:00 debug.log
-rw-r--r--.  1 netdata netdata 264102 Jan 22 16:05 error.log
-rw-r--r--.  1 netdata netdata   8508 Jan 22 16:10 health.log
```

+ `access.log` : Les logs d'accès, genre un *type* qui se co, j'imagine ?
+ `debug.log` : Débogage pour les devs
+ `error.log` : Les erreurs
+ `health.log` : wat

### Configurer Netdata pour qu'il vous envoie des alertes

J'ai créé une intégration sur mon serveur poubelle et je l'ai connecté à netdata :

`/etc/netdata/health_alarm_notify.conf`
```sh
[chval@db netdata]$ cat /etc/netdata/health_alarm_notify.conf | grep DISCORD
SEND_DISCORD="YES"
DISCORD_WEBHOOK_URL="<le lien du webhook>"
DEFAULT_RECIPIENT_DISCORD="tp"
[chval@db netdata]$
```

### Vérifier que les alertes fonctionnent

J'ai installé ces deux paquets :
```sh
[chval@db netdata]$ sudo dnf install stress
[...]
[chval@db netdata]$ sudo dnf install stress-ng
[...]
```