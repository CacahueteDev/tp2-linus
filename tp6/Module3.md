# Module 3 : Fail2Ban

### Faites en sorte que

J'ai donc installé Fail2Ban et je l'ai configuré
```sh
[chval@db ~]$ sudo dnf install epel-release -y
[sudo] password for chval:
Rocky Linux 9 - BaseOS                                                                  4.2 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                               6.4 kB/s | 4.1 kB     00:00
Rocky Linux 9 - Extras                                                                  4.7 kB/s | 2.9 kB     00:00
Dependencies resolved.
========================================================================================================================
 Package                         Architecture              Version                      Repository                 Size
========================================================================================================================
Installing:
 epel-release                    noarch                    9-4.el9                      extras                     19 k

Transaction Summary
========================================================================================================================
Install  1 Package

Total download size: 19 k
Installed size: 25 k
Downloading Packages:
epel-release-9-4.el9.noarch.rpm                                                          75 kB/s |  19 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                    28 kB/s |  19 kB     00:00
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : epel-release-9-4.el9.noarch                                                                    1/1
  Running scriptlet: epel-release-9-4.el9.noarch                                                                    1/1
Many EPEL packages require the CodeReady Builder (CRB) repository.
It is recommended that you run /usr/bin/crb enable to enable the CRB repository.

  Verifying        : epel-release-9-4.el9.noarch                                                                    1/1

Installed:
  epel-release-9-4.el9.noarch

Complete!
[chval@db ~]$ sudo dnf install fail2ban -y
Extra Packages for Enterprise Linux 9 - x86_64                                          207 kB/s |  13 MB     01:04
Last metadata expiration check: 0:00:11 ago on Sun 22 Jan 2023 03:30:41 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                            Architecture           Version                         Repository              Size
========================================================================================================================
Installing:
 fail2ban                           noarch                 1.0.1-2.el9                     epel                   8.5 k
Installing dependencies:
 esmtp                              x86_64                 1.2-19.el9                      epel                    52 k
 fail2ban-firewalld                 noarch                 1.0.1-2.el9                     epel                   8.7 k
 fail2ban-sendmail                  noarch                 1.0.1-2.el9                     epel                    11 k
 fail2ban-server                    noarch                 1.0.1-2.el9                     epel                   442 k
 libesmtp                           x86_64                 1.0.6-24.el9                    epel                    66 k
 liblockfile                        x86_64                 1.14-10.el9                     baseos                  27 k
 python3-systemd                    x86_64                 234-18.el9                      baseos                  83 k

Transaction Summary
========================================================================================================================
Install  8 Packages

Total download size: 699 k
Installed size: 2.0 M
Downloading Packages:
(1/8): fail2ban-1.0.1-2.el9.noarch.rpm                                                   43 kB/s | 8.5 kB     00:00
(2/8): fail2ban-firewalld-1.0.1-2.el9.noarch.rpm                                         41 kB/s | 8.7 kB     00:00
(3/8): fail2ban-sendmail-1.0.1-2.el9.noarch.rpm                                         103 kB/s |  11 kB     00:00
(4/8): esmtp-1.2-19.el9.x86_64.rpm                                                      164 kB/s |  52 kB     00:00
(5/8): libesmtp-1.0.6-24.el9.x86_64.rpm                                                 359 kB/s |  66 kB     00:00
(6/8): python3-systemd-234-18.el9.x86_64.rpm                                            214 kB/s |  83 kB     00:00
(7/8): liblockfile-1.14-10.el9.x86_64.rpm                                               118 kB/s |  27 kB     00:00
(8/8): fail2ban-server-1.0.1-2.el9.noarch.rpm                                           704 kB/s | 442 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   328 kB/s | 699 kB     00:02
Extra Packages for Enterprise Linux 9 - x86_64                                          1.6 MB/s | 1.6 kB     00:00
Importing GPG key 0x3228467C:
 Userid     : "Fedora (epel9) <epel@fedoraproject.org>"
 Fingerprint: FF8A D134 4597 106E CE81 3B91 8A38 72BF 3228 467C
 From       : /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-9
Key imported successfully
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : liblockfile-1.14-10.el9.x86_64                                                                 1/8
  Installing       : python3-systemd-234-18.el9.x86_64                                                              2/8
  Installing       : fail2ban-server-1.0.1-2.el9.noarch                                                             3/8
  Running scriptlet: fail2ban-server-1.0.1-2.el9.noarch                                                             3/8
  Installing       : fail2ban-firewalld-1.0.1-2.el9.noarch                                                          4/8
  Installing       : libesmtp-1.0.6-24.el9.x86_64                                                                   5/8
  Installing       : esmtp-1.2-19.el9.x86_64                                                                        6/8
  Running scriptlet: esmtp-1.2-19.el9.x86_64                                                                        6/8
  Installing       : fail2ban-sendmail-1.0.1-2.el9.noarch                                                           7/8
  Installing       : fail2ban-1.0.1-2.el9.noarch                                                                    8/8
  Running scriptlet: fail2ban-1.0.1-2.el9.noarch                                                                    8/8
  Verifying        : esmtp-1.2-19.el9.x86_64                                                                        1/8
  Verifying        : fail2ban-1.0.1-2.el9.noarch                                                                    2/8
  Verifying        : fail2ban-firewalld-1.0.1-2.el9.noarch                                                          3/8
  Verifying        : fail2ban-sendmail-1.0.1-2.el9.noarch                                                           4/8
  Verifying        : fail2ban-server-1.0.1-2.el9.noarch                                                             5/8
  Verifying        : libesmtp-1.0.6-24.el9.x86_64                                                                   6/8
  Verifying        : python3-systemd-234-18.el9.x86_64                                                              7/8
  Verifying        : liblockfile-1.14-10.el9.x86_64                                                                 8/8

Installed:
  esmtp-1.2-19.el9.x86_64                 fail2ban-1.0.1-2.el9.noarch           fail2ban-firewalld-1.0.1-2.el9.noarch
  fail2ban-sendmail-1.0.1-2.el9.noarch    fail2ban-server-1.0.1-2.el9.noarch    libesmtp-1.0.6-24.el9.x86_64
  liblockfile-1.14-10.el9.x86_64          python3-systemd-234-18.el9.x86_64

Complete!
[chval@db ~]$ systemctl status fail2ban.service
○ fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; disabled; vendor preset: disabled)
     Active: inactive (dead)
       Docs: man:fail2ban(1)
[chval@db ~]$ cd /etc/fail2ban
[chval@db fail2ban]$ head -20 jail.conf
#
# WARNING: heavily refactored in 0.9.0 release.  Please review and
#          customize settings for your setup.
#
# Changes:  in most of the cases you should not modify this
#           file, but provide customizations in jail.local file,
#           or separate .conf files under jail.d/ directory, e.g.:
#
# HOW TO ACTIVATE JAILS:
#
# YOU SHOULD NOT MODIFY THIS FILE.
#
# It will probably be overwritten or improved in a distribution update.
#
# Provide customizations in a jail.local file or a jail.d/customisation.local.
# For example to change the default bantime for all jails and to enable the
# ssh-iptables jail the following (uncommented) would appear in the .local file.
# See man 5 jail.conf for details.
#
# [DEFAULT]
[chval@db fail2ban]$ sudo cp jail.conf jail.local
[chval@db fail2ban]$ sudo nano jail.local
[chval@db fail2ban]$ sudo systemctl enable --now fail2ban
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[chval@db fail2ban]$ sudo systemctl start fail2ban
[chval@db fail2ban]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Sun 2023-01-22 15:34:29 CET; 9s ago
       Docs: man:fail2ban(1)
    Process: 1803 ExecStartPre=/bin/mkdir -p /run/fail2ban (code=exited, status=0/SUCCESS)
   Main PID: 1804 (fail2ban-server)
      Tasks: 5 (limit: 11118)
     Memory: 14.1M
        CPU: 128ms
     CGroup: /system.slice/fail2ban.service
             └─1804 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Jan 22 15:34:29 db.tp5.linux systemd[1]: Starting Fail2Ban Service...
Jan 22 15:34:29 db.tp5.linux systemd[1]: Started Fail2Ban Service.
Jan 22 15:34:29 db.tp5.linux fail2ban-server[1804]: 2023-01-22 15:34:29,107 fail2ban.configreader   [1804]: WARNING 'al>
Jan 22 15:34:29 db.tp5.linux fail2ban-server[1804]: Server ready
[chval@db fail2ban]$
```

`jail.local`
```
// TODO: Faut récup ce fichier
```

J'ai essayé de me connecter et de me faire ban, effectivement, ça a marché
```
C:\Users\Cacahuète>ssh chval@192.168.56.112
chval@192.168.56.112's password:
Permission denied, please try again.
chval@192.168.56.112's password:
Permission denied, please try again.
chval@192.168.56.112's password:
chval@192.168.56.112: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).

C:\Users\Cacahuète>ssh chval@192.168.56.112
ssh: connect to host 192.168.56.112 port 22: Connection timed out
```

Et j'ai pu vérifier que mon PC s'est fait ban de la vm par Fail2Ban :
```sh
[chval@db ~] sudo fail2ban—client status sshd
[sudo] password for chval :
Status for the jail: sshd
|— Filter
|— Currently failed: 0
|— Total failed: 3
Journal matches :
Actions
|- Currently banned: 1
|- Total banned: 1
|- Banned IP list: 192.168.56.109
```

> J'ai dû reconstruire la sortie, c'est mon PC qui s'est fait ban du coup pas de copier coller avec SSH (bravo !)

*Comment je fais pour le déban aled* 😭

*Du coup je passe directement dessus...*
### Lever le ban avec une commande liée à fail2ban

J'ai exécuté une commande pour déban mon PC : 
```sh
[chval@db ~]$ sudo fail2ban-client set sshd unbanip 192.168.56.109
1
[chval@db ~]$ 
```

*Hein ?*
*D'accord*