# Module 2 : Sauvegarde du système de fichiers

# I. Script de backup

## 1. Ecriture du script

### Ecrire le script bash

Voici un simple script qui backup toute l'installation de nextcloud (tout est important ! Oui même *la gueule qu'il a le site*)
```sh
[chval@web srv]$ cat tp6_backup.sh
#/bin/bash

cd /srv
mkdir backup

date=`date +%y%m%d%H%M%S`
filename=nextcloud_$date.tar.gz

echo Backing up to $filename...

tar -zcvf backup/$filename /var/www/tp5_nextcloud/ > /dev/null
```

> Anecdote : j'ai essayé d'utiliser `gzip` mais ça a tourné au vinaigre... (il m'a compressé tous les fichiers du dossier de nextcloud **individuellement** au lieu de tout mettre dans une archive, tout *en supprimant les anciens fichiers*. Quel mec sympa !)

*La gueule du site*
![J'ai vraiment fait de la merde](files/site.png)

## 2. Clean it

**Utiliser des variables** :

Voilà à quoi ressemble le script maintenant :
```sh
[chval@web srv]$ cat tp6_backup.sh
#/bin/bash

cd /srv
mkdir backup

date=`date +%y%m%d%H%M%S`
filename=nextcloud_$date.tar.gz

echo Backing up to $filename...

tar -zcvf backup/$filename /var/www/tp5_nextcloud/ > /dev/null
```

Oui, effectivement, *rien a changé*.

**Commentez le script** :
Voilà à quoi ressemble le script maintenant :
```sh
[chval@web srv]$ cat tp6_backup.sh
#/bin/bash

# Script fait le 21/01/2023
# Par Cacahuète
# Description: Il backup le nextcloud

# on se fout dans le dossier /srv
cd /srv
# on créé le dossier backup pour éviter que tar nous engueule
mkdir backup

# on récupère la date format ricain
date=`date +%y%m%d%H%M%S`
# on construit le nom du fichier
filename=nextcloud_$date.tar.gz

# on tient l'utilisateur au courant
echo Backing up to $filename...

# on compresse les fichiers importants
tar -zcvf backup/$filename /var/www/tp5_nextcloud/data /var/www/tp5_nextcloud/config > /dev/null
```

J'en ai profité pour compresser **que les dossiers importants**. Ils ont parlé d'un dossier `theme` aussi, mais moi je l'ai pas donc voilà.

**Environnement d'exécution du script**

```sh
[chval@web tp5_nextcloud]$ sudo useradd backup -s /usr/bin/nologin -d /srv/backup
useradd: Warning: missing or non-executable shell '/usr/bin/nologin'
useradd: warning: the home directory /srv/backup already exists.
useradd: Not copying any file from skel directory into it.
[chval@web tp5_nextcloud]$ sudo chown backup:backup /srv/backup
[chval@web tp5_nextcloud]$
```

Le dossier appartient à backup :
```sh
[chval@web tp5_nextcloud]$ ls -all /srv/backup
total 391416
drwxr-xr-x. 2 backup backup       103 Jan 21 12:51 .
drwxr-xr-x. 3 root   root          62 Jan 21 12:33 ..
-rw-r--r--. 1 root   root   187244950 Jan 21 12:32 nextcloud_230121123235.tar.gz
-rw-r--r--. 1 root   root   187244950 Jan 21 12:34 nextcloud_230121123352.tar.gz
-rw-r--r--. 1 root   root    26312399 Jan 21 12:51 trouducu.tar.gz
[chval@web tp5_nextcloud]$
```

Pas les fichiers par contre...

```sh
-rw-r--r--. 1 root   root    26312399 Jan 21 12:51 trouducu.tar.gz
```

Fait pas attention à celui-là *tkt*.

**Tester l'exécution du script en tant que l'utilisateur `backup`**

Après un peu de bidouille (il avait pas les droits), ça a marché.
```sh
[chval@web tp5_nextcloud]$ sudo -u backup /srv/tp6_backup.sh
mkdir: cannot create directory ‘backup’: File exists
Backing up to nextcloud_230121130049.tar.gz...
tar: Removing leading `/' from member names
tar: /var/www/tp5_nextcloud/data: Cannot open: Permission denied
tar: Removing leading `/' from hard link targets
tar: /var/www/tp5_nextcloud/config/config.php: Cannot open: Permission denied
tar: Exiting with failure status due to previous errors
[chval@web tp5_nextcloud]$ ls -all /var/www/tp5_nextcloud/
total 140
drwxr-xr-x. 15 apache apache  4096 Jan 21 12:04 .
drwxr-xr-x.  6 root   root      88 Jan 21 12:01 ..
drwxr-xr-x. 47 apache apache  4096 Jan 21 12:04 3rdparty
drwxr-xr-x. 50 apache apache  4096 Oct  6 14:44 apps
-rw-r--r--.  1 apache apache 19327 Oct  6 14:42 AUTHORS
drwxr-xr-x.  2 apache apache    66 Jan 21 12:04 config
-rw-r--r--.  1 apache apache  4095 Oct  6 14:42 console.php
-rw-r--r--.  1 apache apache 34520 Oct  6 14:42 COPYING
drwxr-xr-x. 23 apache apache  4096 Jan 21 12:04 core
-rw-r--r--.  1 apache apache  6317 Oct  6 14:42 cron.php
drwxrwx---.  5 apache apache   140 Jan 21 12:04 data
drwxr-xr-x.  2 apache apache  8192 Jan 21 12:04 dist
-rw-r--r--.  1 apache apache  3345 Jan 17 15:41 .htaccess
-rw-r--r--.  1 apache apache   156 Oct  6 14:42 index.html
-rw-r--r--.  1 apache apache  3456 Oct  6 14:42 index.php
drwxr-xr-x.  6 apache apache   125 Jan 21 12:04 lib
-rw-r--r--.  1 apache apache   283 Oct  6 14:42 occ
drwxr-xr-x.  2 apache apache    23 Jan 21 12:04 ocm-provider
drwxr-xr-x.  2 apache apache    55 Jan 21 12:04 ocs
drwxr-xr-x.  2 apache apache    23 Jan 21 12:04 ocs-provider
-rw-r--r--.  1 apache apache  3139 Oct  6 14:42 public.php
-rw-r--r--.  1 apache apache  5426 Oct  6 14:42 remote.php
drwxr-xr-x.  4 apache apache   133 Jan 21 12:04 resources
-rw-r--r--.  1 apache apache    26 Oct  6 14:42 robots.txt
-rw-r--r--.  1 apache apache  2452 Oct  6 14:42 status.php
drwxr-xr-x.  3 apache apache    35 Jan 21 12:04 themes
drwxr-xr-x.  2 apache apache    43 Jan 21 12:04 updater
-rw-r--r--.  1 apache apache   101 Oct  6 14:42 .user.ini
-rw-r--r--.  1 apache apache   387 Oct  6 14:47 version.php
[chval@web tp5_nextcloud]$ sudo usermod -aG apache backup
[chval@web tp5_nextcloud]$ sudo -u backup /srv/tp6_backup.sh
mkdir: cannot create directory ‘backup’: File exists
Backing up to nextcloud_230121130137.tar.gz...
tar: Removing leading `/' from member names
tar: Removing leading `/' from hard link targets
[chval@web tp5_nextcloud]$
```

## 3. Service et timer

### Créez un service

```sh
[chval@web srv]$ cat /lib/systemd/system/backup.service
[Unit]
Description=Nextcloud backup service

[Service]
ExecStart=/srv/tp6_backup.sh
Type=oneshot
User=backup
Group=apache

[Install]
WantedBy=multi-user.target
[chval@web srv]$
```

Assurez-vous qu'il fonctionne :

*J'ai dû modifier le fichier de service en mettant /bin/bash avant le chemin du script, apparemment systemd en a rien à foutre du shebang*

```sh
[chval@web srv]$ cat /lib/systemd/system/backup.service
[Unit]
Description=Nextcloud backup service

[Service]
ExecStart=/bin/bash /srv/tp6_backup.sh
Type=oneshot
User=backup
Group=apache

[Install]
WantedBy=multi-user.target
[chval@web srv]$
```

Enfin, j'ai retesté :

```sh
[chval@web srv]$ sudo systemctl start backup
[chval@web srv]$ sudo systemctl status backup
○ backup_nc.service - Nextcloud backup service
     Loaded: loaded (/usr/lib/systemd/system/backup.service; disabled; vendor preset: disabled)
     Active: inactive (dead)

Jan 21 13:07:42 web.tp5.linux systemd[1]: backup.service: Main process exited, code=exited, status=203/EXEC
Jan 21 13:07:42 web.tp5.linux systemd[1]: backup.service: Failed with result 'exit-code'.
Jan 21 13:07:42 web.tp5.linux systemd[1]: Failed to start Nextcloud backup service.
Jan 21 13:07:55 web.tp5.linux systemd[1]: Starting Nextcloud backup service...
Jan 21 13:07:55 web.tp5.linux bash[2058]: mkdir: cannot create directory ‘backup’: File exists
Jan 21 13:07:55 web.tp5.linux bash[2057]: Backing up to nextcloud_230121130755.tar.gz...
Jan 21 13:07:55 web.tp5.linux bash[2060]: tar: Removing leading `/' from member names
Jan 21 13:07:55 web.tp5.linux bash[2060]: tar: Removing leading `/' from hard link targets
Jan 21 13:07:56 web.tp5.linux systemd[1]: backup.service: Deactivated successfully.
Jan 21 13:07:56 web.tp5.linux systemd[1]: Finished Nextcloud backup service.
[chval@web srv]$
```

### Créez un timer

```sh
[chval@web srv]$ cat /lib/systemd/system/backup.timer
[Unit]
Description=Run service backup.service

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
[chval@web srv]$
```

### Activez l'utilisation du _timer_

```sh
[chval@web srv]$ sudo systemctl start backup.timer
[chval@web srv]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /usr/lib/systemd/system/backup.timer.
[chval@web srv]$ sudo systemctl status backup.timer
● backup.timer - Run service backup.service
     Loaded: loaded (/usr/lib/systemd/system/backup.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Sat 2023-01-21 13:13:27 CET; 7s ago
      Until: Sat 2023-01-21 13:13:27 CET; 7s ago
    Trigger: Sun 2023-01-22 04:00:00 CET; 14h left
   Triggers: ● backup.service

Jan 21 13:13:27 web.tp5.linux systemd[1]: Started Run service backup.service.
[chval@web srv]$
```

Il apparaît dans la liste des timers :
```sh
[chval@web srv]$ sudo systemctl list-timers | grep backup.timer
Sun 2023-01-22 04:00:00 CET 14h left   n/a                         n/a          backup.timer                 backup.service
[chval@web srv]$
```

# II. NFS

## 1. Serveur NFS

### Préparer un dossier à partager sur le réseau

```sh
[chval@storage ~]$ sudo mkdir /srv/nfs_shares
[sudo] password for chval:
[chval@storage ~]$ sudo mkdir /srv/nfs_shares/web.tp6.linux/
```

### Installer le serveur NFS
```sh
[chval@storage ~]$ sudo dnf install nfs-utils
Rocky Linux 9 - BaseOS                                                                  5.3 kB/s | 3.6 kB     00:00
Rocky Linux 9 - AppStream                                                               4.7 kB/s | 4.1 kB     00:00
Rocky Linux 9 - AppStream                                                               1.0 MB/s | 6.4 MB     00:06
Rocky Linux 9 - Extras                                                                  5.0 kB/s | 2.9 kB     00:00
Rocky Linux 9 - Extras                                                                   20 kB/s | 8.5 kB     00:00
Package nfs-utils-1:2.5.4-15.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
[chval@storage ~]$ sudo nano /etc/exports
[chval@storage ~]$ cat /etc/exports
/storage 192.168.56.110(rw,sync,no_subtree_check)
/srv/nfs_shares/web.tp6.linux 192.168.56.116(rw,sync,no_subtree_check)
[chval@storage ~]$ sudo systemctl status nfs
Unit nfs.service could not be found.
[chval@storage ~]$ sudo systemctl status nfs-utils
○ nfs-utils.service - NFS server and client services
     Loaded: loaded (/usr/lib/systemd/system/nfs-utils.service; static)
     Active: inactive (dead)
[chval@storage ~]$ sudo systemctl enable --now nfs-utils
The unit files have no installation config (WantedBy=, RequiredBy=, Also=,
Alias= settings in the [Install] section, and DefaultInstance= for template
units). This means they are not meant to be enabled using systemctl.

Possible reasons for having this kind of units are:
• A unit may be statically enabled by being symlinked from another unit's
  .wants/ or .requires/ directory.
• A unit's purpose may be to act as a helper for some other unit which has
  a requirement dependency on it.
• A unit may be started when needed via activation (socket, path, timer,
  D-Bus, udev, scripted systemctl call, ...).
• In case of template units, the unit is meant to be enabled with some
  instance name specified.
[chval@storage ~]$ sudo systemctl status nfs-utils
● nfs-utils.service - NFS server and client services
     Loaded: loaded (/usr/lib/systemd/system/nfs-utils.service; static)
     Active: active (exited) since Sat 2023-01-21 13:20:12 CET; 15s ago
    Process: 1403 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 1403 (code=exited, status=0/SUCCESS)
        CPU: 1ms

Jan 21 13:20:12 storage.tp4.linux systemd[1]: Starting NFS server and client services...
Jan 21 13:20:12 storage.tp4.linux systemd[1]: Finished NFS server and client services.
[chval@storage ~]$
```

## 2. Client NFS

```sh
[chval@web srv]$ sudo dnf install nfs-utils
[sudo] password for chval:
Last metadata expiration check: 1:06:36 ago on Sat 21 Jan 2023 12:16:37 PM CET.
Dependencies resolved.
========================================================================================================================
 Package                         Architecture            Version                          Repository               Size
========================================================================================================================
Installing:
 nfs-utils                       x86_64                  1:2.5.4-15.el9                   baseos                  421 k
Installing dependencies:
 gssproxy                        x86_64                  0.8.4-4.el9                      baseos                  108 k
 keyutils                        x86_64                  1.6.1-4.el9                      baseos                   62 k
 libev                           x86_64                  4.33-5.el9                       baseos                   52 k
 libnfsidmap                     x86_64                  1:2.5.4-15.el9                   baseos                   61 k
 libtirpc                        x86_64                  1.3.3-0.el9                      baseos                   92 k
 libverto-libev                  x86_64                  0.3.2-3.el9                      baseos                   13 k
 python3-pyyaml                  x86_64                  5.4.1-6.el9                      baseos                  191 k
 quota                           x86_64                  1:4.06-6.el9                     baseos                  190 k
 quota-nls                       noarch                  1:4.06-6.el9                     baseos                   78 k
 rpcbind                         x86_64                  1.2.6-5.el9                      baseos                   56 k
 sssd-nfs-idmap                  x86_64                  2.7.3-4.el9_1.1                  baseos                   39 k

Transaction Summary
========================================================================================================================
Install  12 Packages

Total download size: 1.3 M
Installed size: 3.8 M
Is this ok [y/N]: y
Downloading Packages:
(1/12): libverto-libev-0.3.2-3.el9.x86_64.rpm                                            26 kB/s |  13 kB     00:00
(2/12): quota-4.06-6.el9.x86_64.rpm                                                     361 kB/s | 190 kB     00:00
(3/12): quota-nls-4.06-6.el9.noarch.rpm                                                 144 kB/s |  78 kB     00:00
(4/12): libtirpc-1.3.3-0.el9.x86_64.rpm                                                 395 kB/s |  92 kB     00:00
(5/12): rpcbind-1.2.6-5.el9.x86_64.rpm                                                  305 kB/s |  56 kB     00:00
(6/12): python3-pyyaml-5.4.1-6.el9.x86_64.rpm                                           821 kB/s | 191 kB     00:00
(7/12): sssd-nfs-idmap-2.7.3-4.el9_1.1.x86_64.rpm                                       538 kB/s |  39 kB     00:00
(8/12): libev-4.33-5.el9.x86_64.rpm                                                     630 kB/s |  52 kB     00:00
(9/12): libnfsidmap-2.5.4-15.el9.x86_64.rpm                                             451 kB/s |  61 kB     00:00
(10/12): gssproxy-0.8.4-4.el9.x86_64.rpm                                                568 kB/s | 108 kB     00:00
(11/12): keyutils-1.6.1-4.el9.x86_64.rpm                                                450 kB/s |  62 kB     00:00
(12/12): nfs-utils-2.5.4-15.el9.x86_64.rpm                                              1.1 MB/s | 421 kB     00:00
------------------------------------------------------------------------------------------------------------------------
Total                                                                                   841 kB/s | 1.3 MB     00:01
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                1/1
  Installing       : libtirpc-1.3.3-0.el9.x86_64                                                                   1/12
  Installing       : libnfsidmap-1:2.5.4-15.el9.x86_64                                                             2/12
  Running scriptlet: rpcbind-1.2.6-5.el9.x86_64                                                                    3/12
  Installing       : rpcbind-1.2.6-5.el9.x86_64                                                                    3/12
  Running scriptlet: rpcbind-1.2.6-5.el9.x86_64                                                                    3/12
Created symlink /etc/systemd/system/multi-user.target.wants/rpcbind.service → /usr/lib/systemd/system/rpcbind.service.
Created symlink /etc/systemd/system/sockets.target.wants/rpcbind.socket → /usr/lib/systemd/system/rpcbind.socket.

  Installing       : keyutils-1.6.1-4.el9.x86_64                                                                   4/12
  Installing       : libev-4.33-5.el9.x86_64                                                                       5/12
  Installing       : libverto-libev-0.3.2-3.el9.x86_64                                                             6/12
  Installing       : gssproxy-0.8.4-4.el9.x86_64                                                                   7/12
  Running scriptlet: gssproxy-0.8.4-4.el9.x86_64                                                                   7/12
  Installing       : python3-pyyaml-5.4.1-6.el9.x86_64                                                             8/12
  Installing       : quota-nls-1:4.06-6.el9.noarch                                                                 9/12
  Installing       : quota-1:4.06-6.el9.x86_64                                                                    10/12
  Running scriptlet: nfs-utils-1:2.5.4-15.el9.x86_64                                                              11/12
  Installing       : nfs-utils-1:2.5.4-15.el9.x86_64                                                              11/12
  Running scriptlet: nfs-utils-1:2.5.4-15.el9.x86_64                                                              11/12
  Installing       : sssd-nfs-idmap-2.7.3-4.el9_1.1.x86_64                                                        12/12
  Running scriptlet: sssd-nfs-idmap-2.7.3-4.el9_1.1.x86_64                                                        12/12
  Verifying        : libverto-libev-0.3.2-3.el9.x86_64                                                             1/12
  Verifying        : quota-1:4.06-6.el9.x86_64                                                                     2/12
  Verifying        : quota-nls-1:4.06-6.el9.noarch                                                                 3/12
  Verifying        : libtirpc-1.3.3-0.el9.x86_64                                                                   4/12
  Verifying        : python3-pyyaml-5.4.1-6.el9.x86_64                                                             5/12
  Verifying        : rpcbind-1.2.6-5.el9.x86_64                                                                    6/12
  Verifying        : libev-4.33-5.el9.x86_64                                                                       7/12
  Verifying        : sssd-nfs-idmap-2.7.3-4.el9_1.1.x86_64                                                         8/12
  Verifying        : gssproxy-0.8.4-4.el9.x86_64                                                                   9/12
  Verifying        : nfs-utils-1:2.5.4-15.el9.x86_64                                                              10/12
  Verifying        : libnfsidmap-1:2.5.4-15.el9.x86_64                                                            11/12
  Verifying        : keyutils-1.6.1-4.el9.x86_64                                                                  12/12

Installed:
  gssproxy-0.8.4-4.el9.x86_64           keyutils-1.6.1-4.el9.x86_64           libev-4.33-5.el9.x86_64
  libnfsidmap-1:2.5.4-15.el9.x86_64     libtirpc-1.3.3-0.el9.x86_64           libverto-libev-0.3.2-3.el9.x86_64
  nfs-utils-1:2.5.4-15.el9.x86_64       python3-pyyaml-5.4.1-6.el9.x86_64     quota-1:4.06-6.el9.x86_64
  quota-nls-1:4.06-6.el9.noarch         rpcbind-1.2.6-5.el9.x86_64            sssd-nfs-idmap-2.7.3-4.el9_1.1.x86_64

Complete!
[chval@web srv]$ sudo mount 192.168.56.113:/srv/nfs_shares/web.tp6.linux /srv/backup
mount.nfs: access denied by server while mounting 192.168.56.113:/srv/nfs_shares/web.tp6.linux
[chval@web srv]$ sudo mount 192.168.56.113:/srv/nfs_shares/web.tp6.linux /srv/backup
[chval@web srv]$ cd /srv/backup
[chval@web backup]$ sudo nano /etc/fstab
[chval@web ~]$ sudo systemctl restart
```

Faites en sorte que le dossier soit monté au démarrage :

```sh
[chval@web srv]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Fri Oct 14 10:50:35 2022
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=c4c593e9-b673-4240-a7e0-17cce31b2977 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
192.168.56.113:/srv/nfs_shares/web.tp6.linux /srv/backup nfs      defaults    0       0
[chval@web srv]$
```

### Tester la restauration des données

Si on doit restaurer des données, on utiliserait cette suite de commande :
```sh
[chval@web tp5_nextcloud]$ cd /srv/backup
[chval@web backup]$ ls -all
# on récupère le fichier le plus récent
[chval@web backup]$ unzip [fichier le plus récent] /var/www/tp5_nextcloud
[chval@web backup]$ sudo systemctl restart httpd
```

Et puis au cas où tiens :

```sh
[chval@web backup]$ sudo reboot
```

> J'ai pas pu réellement tester car j'ai eu des gros soucis de permissions, genre j'ai voulu faire appartenir `/srv/backup` à `nobody` et le groupe `apache`, car l'utilisateur `backup` est dedans, mais ça a pas marché... 
> J'imagine qu'on peut pas faire appartenir un dossier à `nobody` et un autre groupe... Maybe...